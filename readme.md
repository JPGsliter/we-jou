# We江海大 · 第三方学生教务信息查询平台

    🥳      😎      🎉

## 项目组成

- [x] backend      **Alpha Stage** 

  运行： `$npm start`

- [x] frontend     **Developing Stage👨‍💻**

---

### 功能

  - core

    1. 成绩查询
    2. 考试安排
    3. 课表

  - append

    1. 学院号码
    2. 作息时间
    3. 教学日历
    4. 班车时刻
    5. 学习交流
    6. 阅读 


