Page({
    data: {
        content: ''
    },
    onReady() {
        const { server: serveInfo } = require('../../utils/config')
        const date = new Date()
        const url = `http://${serveInfo.url}:${serveInfo.port}/getarticle`
        wx.request({
            url,
            data: {
                date: date.toDateString(),
            },
            success: ({ data: res }) => {
                if (res.length == 0)
                    wx.showToast({
                        title: '今天没内容了，明天再看看吧🤗！',
                        icon: 'none',
                    })
                else {
                    this.setData({
                        content: res[0].content
                    })
                }
            },
            fail: (reason) => {
                console.log('reason :>> ', reason)
                wx.showToast({
                    title: '服务器出错了😖！',
                    icon: 'none',
                })
            },
        })
    },
})
