Page({
    data: {
        xuehao: '',
        examinfo: [],
    },
    onLoad() {
        const xuehao = wx.getStorageSync('account')
        this.setData({
            xuehao,
        })
    },
    getExamEvents() {
        if (this.data.xuehao.length != 10) {
            wx.showToast({
                title: '学号长度为10位，重新输入吧🤗！',
                icon: 'none',
            })
            return
        }
        const url = 'http://localhost:3000/getexaminfo'

        wx.request({
            url,
            method: 'get',
            //   header: {
            //     "Content-Type":"application/json;charset=UTF-8"
            //   },
            data: {
                xh: this.data.xuehao,
            },
            success: (res) => {
                this.printInfo(res.data)
            },
            fail: (reason) => {
                console.log('reason :>> ', reason)
            },
        })
    },
    clearInput() {
        this.setData({
            xuehao: '',
        })
    },
    printInfo(info) {
        if (info.length == 0)
            wx.showToast({
                title: '暂无任何考试信息哦🧐！',
                icon: 'none',
            })
        this.setData({
            examinfo:info
        })

    },
    foo() {},
})
