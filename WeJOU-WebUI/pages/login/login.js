Page({
    data: {
        baseUrl: '',
        account: '',
        pswd: '',
        validCode: '',
        // 图片URL
        uValidCode: undefined,
    },

    // 生命周期方法
    onLoad: function (options) {
        const { server } = require('../../utils/config')
        const basicUrl = `http://${server.url}:${server.port}`
        this.data.baseUrl = basicUrl
        this.freshCode()
        console.log('wx :>> ', wx.getStorageSync('pswd'))
        this.setData({
            account: wx.getStorageSync('account'),
            pswd: wx.getStorageSync('pswd'),
        })
    },
    login() {
        const postData = {
            stateValue: this.data.stateValue,
            randomUrl: this.data.token,
            txtUserName: this.data.account,
            password: this.data.pswd,
            verifyCode: this.data.validCode,
            RadioButtonList1: '学生',
        }
        wx.showLoading({
            title: '登录中😪...',
        })
        wx.request({
            url: `${this.data.baseUrl}/zflogin`,
            method: 'post',
            data: postData,
            success: ({ data: res }) => {
                if ('status' in res) {
                    wx.showToast({
                        title: '请输入正确信息😢!',
                        icon: 'none',
                        duration: 1500,
                    })
                    this.freshCode()
                    wx.clearStorage({
                        complete: (res) => {
                            console.log('msg: clear all info on local')
                        },
                    })
                    return
                }

                wx.showToast({
                    title: '登录成功😀！',
                    icon: 'success',
                    duration: 1500,
                })
                // 存储成绩信息
                wx.setStorage({
                    data: res.scoreinfo,
                    key: 'scoreinfo',
                })
                // 存储课表信息
                wx.setStorageSync({
                    data: res.tableinfo,
                    key: 'tableinfo',
                })
                // 存储用户
                wx.setStorage({
                    data: this.data.account,
                    key: 'account',
                })
                // 存储密码
                wx.setStorage({
                    data: this.data.pswd,
                    key: 'pswd',
                })

                wx.switchTab({
                    url: '/pages/menu/menu',
                })
            },
            fail(reason) {
                console.log('reason :>> ', reason)
                wx.showToast({
                    title: '网络出错了😅！',
                    icon: 'none',
                    duration: 1500,
                })

                this.freshCode()
                wx.clearStorage({
                    complete: (res) => {
                        console.log('msg: clear all info on local')
                    },
                })
            },
        })
    },
    freshCode() {
        // 获取登录令牌
        wx.request({
            url: `${this.data.baseUrl}/getview`,
            method: 'get',
            success: (data) => {
                const { data: res } = data
                this.setData({
                    stateValue: res.stateValue,
                    token: res.token,
                })
                getValidCodePic()
            },
            fail(err) {
                console.error(err)
            },
        })

        var getValidCodePic = () => {
            const validCodeurl = `http://zfxk.jou.edu.cn${this.data.token}/CheckCode.aspx`
            // 获取验证码图片
            this.setData({
                uValidCode: validCodeurl,
                validCode: '',
            })
        }
    },
    foo() {},
})
