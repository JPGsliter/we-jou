// pages/classtb/classtb.js
Page({
    data: {
        // init
        dialogShow: false,
    buttons: [{
      text: '取消'
    }, {
      text: '确定'
    }],
    colorArrays: ["#85B8CF", "#90C652", "#D8AA5A", "#FC9F9D", "#0A9A84", "#61BC69", "#12AEF3", "#E29AAD"],
        // data
        compose:[],
        weeks: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],
        week: 1,
        weekIndex: 9,
        current: []
    },
    onReady() {
        // 将课程信息存到当前data.compose
        this.data.compose = wx.getStorageSync('tableinfo')
        // 设置周数
        let {keys} = wx.getStorageInfoSync()
        if(keys.includes('week'))
            this.setData({
                week: wx.getStorageSync('week')
            })  
        // 获取当前周的课程信息 => current
            this.setData ({
                current: this.data.compose.filter(e => e.time.includes(this.data.week))
            })
    },
    // 切换数据重新刷新
    refresh() {
        console.log('this.data.weekIndex :>> ', this.data.weekIndex);
        this.setData({
          week: this.data.weeks[this.data.weekIndex]
        })
        this.setData ({
            current: this.data.compose.filter(e => e.time.includes(this.data.week))
        })

        wx.setStorageSync('week', this.data.week)
    },
    showCardView: function () {
        this.setData({
          dialogShow: true
        })
      },
      tapDialogButton(e) {
        this.setData({
          dialogShow: false,
          showOneButtonDialog: false
        })
      },
})
