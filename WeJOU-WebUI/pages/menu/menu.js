Page({
    data: {
        pictures: [
            {
                url: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fedufo.cn%2Fupload%2F2019-09%2F156914509808878700.jpg&refer=http%3A%2F%2Fedufo.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1624014976&t=efa648205e032381178ab1d3bc96163e',
            },
            {
                url: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F10380581643%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1624014976&t=25313dffe3aa0b447eaf16ba86e66e6e',
            },
            {
                url: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fxyzh.jou.edu.cn%2Fimages%2F18%2F06%2F05%2F1tl3o2w3hg%2F20131211151747_0566.jpg&refer=http%3A%2F%2Fxyzh.jou.edu.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1624014976&t=71ba6ee363102aab36342b20f85de69d',
            },
            {
                url: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp0.itc.cn%2Fq_70%2Fimages03%2F20200725%2F2bedae05855549a1beac423e0f67b891.jpeg&refer=http%3A%2F%2Fp0.itc.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1624014976&t=355aaad90d15c59bd2705e6da9c1688d',
            },
            {
                url: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp0.itc.cn%2Fq_70%2Fimages03%2F20200620%2F2934e69ea1a54c67a79424310fd992b5.jpeg&refer=http%3A%2F%2Fp0.itc.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1624014976&t=bbd095966c30b1b1ffea3b50cdf41963',
            },
        ],
    },
    goExamTime() {
        wx.navigateTo({
            url: '/pages/examtime/examtime',
        })
    },

    goScore() {
        wx.navigateTo({
            url: '/pages/score/score',
        })
    },

    goWords() {
        wx.navigateTo({
            url: '/pages/words/words',
        })
    },
    goCalendar() {
        wx.navigateTo({
            url: '/pages/calendar/calendar',
        })
    },
    goTimelist() {
        wx.navigateTo({
            url: '/pages/timelist/timelist',
        })
    },
    goPhone() {
        wx.navigateTo({
            url: '/pages/phone/phone',
        })
    },
})
