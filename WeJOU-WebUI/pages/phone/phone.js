
Page({

    /**
     * 页面的初始数据
     */
    data: {
      list: [
        {
          id: 'form',
          name: '教务处',
          open: false,
          pages: [
            {
              unit:'处长',
              address:'文渊楼南楼203',
              phone:'85895101'
            },
            {
              unit: '副处长',
              address: '文渊楼南楼205',
              phone: '85895107'
            },
            {
              unit: '副处长',
              address: '文渊楼南楼205',
              phone: '85895103'
            },
            {
              unit: '副处长',
              address: '文渊楼南楼202',
              phone: '85895113'
            },
            {
              unit: '副处长',
              address: '文渊楼南楼121',
              phone: '85895116'
            },
            {
              unit: '副处长',
              address: '文渊楼南楼121',
              phone: '85895118'
            },
            {
              unit: '教务科',
              address: '文渊楼南楼201',
              phone: '85895104'
            },
            {
              unit: '学籍科',
              address: '文渊楼南楼201',
              phone: '85895111'
            },
            {
              unit: '考试管理科',
              address: '文渊楼南楼204',
              phone: '85895111'
            },
            {
              unit: '实践科',
              address: '文渊楼南楼201',
              phone: '85895109'
            },
            {
              unit: '教研科',
              address: '文渊楼南楼207',
              phone: '85895105'
            },
            {
              unit: '通灌校区教学办',
              address: '格物楼208',
              phone: '85523096'
            }
  
          ]
        },
        {
          id: 'widget',
          name: '各教学单位教学办',
          open: false,
          pages: [
            {
              unit: '机械与海洋工程学院',
              address: '机械北楼308',
              phone: '85895327'
            },
            {
              unit: '土木与港海工程学院',
              address: '土木北楼320',
              phone: '85895348'
            },
            {
              unit: '电子工程学院',
              address: '电子楼201',
              phone: '85895366'
            },
            {
              unit: '海洋生命与水产学院',
              address: '海洋楼南楼211',
              phone: '85895427'
            },
            {
              unit: '化学工程学院',
              address: '教学主楼805',
              phone: '85895409'
            },
            {
              unit: '商学院',
              address: '教学主楼603',
              phone: '85895757'
            },
            {
              unit: '文学院',
              address: '文渊楼南楼313',
              phone: '85895466'
            },
            {
              unit: '外国语学院',
              address: '文渊楼北楼234（专业外语）',
              phone: '85895486'
            },
            {
              unit: '外国语学院',
              address: '文渊楼北楼236（公共外语）',
              phone: '85895486'
            },
            {
              unit: '理学院',
              address: '文渊楼北楼338',
              phone: '85895506'
            },
            {
              unit: '计算机工程学院',
              address: '计算机楼317',
              phone: '85895387'
            },
            {
              unit: '测绘与海洋信息学院',
              address: '测绘楼400',
              phone: '85895587'
            },
            {
              unit: '法学院',
              address: '文渊楼南楼227',
              phone: '85895545'
            },
            {
              unit: '马克思主义学院',
              address: '文渊楼南楼139',
              phone: '85895812'
            },
            {
              unit: '艺术学院',
              address: '艺术楼410',
              phone: '85895526'
            },
            {
              unit: '体育学院',
              address: '主运动场216',
              phone: '85895564'
            },
            {
              unit: '药学院',
              address: '教学主楼1103',
              phone: '85895790'
            },
            {
              unit: '海洋资源与环境学院',
              address: '东区D5-6工作站208',
              phone: '85895908'
            },
            {
              unit: '创新创业学院',
              address: '文渊楼南楼121',
              phone: '85895128'
            },
            {
              unit: '工训中心',
              address: '综合实验楼南楼302',
              phone: '85895302'
            },
            {
              unit: '宋跳校区',
              address: '厚德楼105',
              phone: '85153225'
            }
          ]
        }
  
      ]
  
    },
  
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
  
    },
  
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
  
    },
  
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
  
    },
  
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
  
    },
  
    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
  
    },
  
    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
  
    },
  
    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
  
    },
    kindToggle: function (e) {
      var id = e.currentTarget.id, list = this.data.list;
      for (var i = 0, len = list.length; i < len; ++i) {
        if (list[i].id == id) {
          list[i].open = !list[i].open
        } else {
          list[i].open = false
        }
      }
      this.setData({
        list: list
      });
    },
    callPhone: function(e) {
      var phoneNum = '0518'+ e.target.dataset.phone;
      wx.makePhoneCall({
        phoneNumber: phoneNum
      });
    }
  })
