Page({
    data: {
        scoreinfo: [],
        grades: [],
        grade: '',
        gradeIndex: 0,
        terms: ['1', '2'],
        term: '1',
        termIndex: 0,
        print: [],
    },

    onReady() {
        const { keys } = wx.getStorageInfoSync()
        if (keys.includes('scoreinfo'))
            this.setData({
                scoreinfo: wx.getStorageSync('scoreinfo').scores,
            })

        const grades = Array.from(new Set(this.data.scoreinfo.map((e) => e[0])))

        this.setData({
            grades,
        })
        if (this.data.grades.length != 0)
            this.setData({
                grade: this.data.grades[0],
            })
        else {
            wx.showToast({
                title: '当前暂无任何成绩数据可查😂！',
                icon: 'none',
            })
        }
        this.printInfo()
    },

    freshGrade() {
        this.setData({
            grade: this.data.grades[this.data.gradeIndex],
        })
        this.printInfo()
    },

    freshTerm() {
        this.setData({
            term: this.data.terms[this.data.termIndex],
        })
        this.printInfo()
    },
    printInfo() {
        this.setData({
            print: this.data.scoreinfo.filter(
                (e) => e.includes(this.data.grade) && e.includes(this.data.term)
            ),
        })

        if (this.data.print.length == 0)
            wx.showToast({
                title: '当前选项暂无数据🤐！',
                icon: 'none',
            })
    },
})
