var choosedsche = [];
var cwSum = [
  {
    name: "起床",
    time: '06:30'
  },
  {
    name: '早操',
    time: '06：40－07：00'
  },
  {
    name: '早餐',
    time: '07：00－07：40'
  },
  {
    name: '预备铃',
    time: '07：35'
  },
  {
    name: '第一节课',
    time: '07：45－08：30'
  },
  {
    name: '第二节课',
    time: '08：35－09：20'
  },
  {
    name: '第三节课',
    time: '09：35－10：20'
  },
  {
    name: '第四节课',
    time: '10：25－11：10'
  },
  {
    name: '第五节课',
    time: '11：15－12：00'
  },
  {
    name: '中餐',
    time: '12：00－12：50'
  },
  {
    name: '预备铃',
    time: '14：20'
  },
  {
    name: '第六节课',
    time: '14：30－15：15'
  },
  {
    name: '第七节课',
    time: '15：20－16：05'
  },
  {
    name: '第八节课',
    time: '16：15－17：00'
  },
  {
    name: '第九节课',
    time: '17：10－17：55'
  },
  {
    name: '第十节课',
    time: '18：00－18：45'
  },
  {
    name: '晚餐',
    time: '18：10－19：00'
  },
  {
    name: '第十一节课',
    time: '19：30－20：15'
  },
  {
    name: '第十二节课',
    time: '20：25－21：10'
  },
  {
    name: '教学楼熄灯',
    time: '22：30'
  },
  {
    name: '宿舍楼熄灯',
    time: '23：00'
  }
  
];

var cwAutumn = [
  {
    name: "起床",
    time: '06:30'
  },
  {
    name: '早操',
    time: '06：40－07：00'
  },
  {
    name: '早餐',
    time: '07：00－08：00'
  },
  {
    name: '预备铃',
    time: '07：35'
  },
  {
    name: '第一节课',
    time: '07：45－08：30'
  },
  {
    name: '第二节课',
    time: '08：35－09：20'
  },
  {
    name: '第三节课',
    time: '09：35－10：20'
  },
  {
    name: '第四节课',
    time: '10：25－11：10'
  },
  {
    name: '第五节课',
    time: '11：15－12：00'
  },
  {
    name: '中餐',
    time: '12：00－12：50'
  },
  {
    name: '预备铃',
    time: '13：50'
  },
  {
    name: '第六节课',
    time: '14：00－14：45'
  },
  {
    name: '第七节课',
    time: '14：50－15：35'
  },
  {
    name: '第八节课',
    time: '15：45－16：30'
  },
  {
    name: '第九节课',
    time: '16：40－17：25'
  },
  {
    name: '第十节课',
    time: '17：30－18：15'
  },
  {
    name: '晚餐',
    time: '17：30－19：00'
  },
  {
    name: '第十一节课',
    time: '19：00－20：45'
  },
  {
    name: '第十二节课',
    time: '19：50－20：35'
  },
  {
    name: '教学楼熄灯',
    time: '22：30'
  },
  {
    name: '宿舍楼熄灯',
    time: '22：45'
  }

];

var tgSum = [
  {
    name: "起床",
    time: '06:30'
  },
  {
    name: '早操',
    time: '06：40－07：00'
  },
  {
    name: '早餐',
    time: '07：00－07：50'
  },
  {
    name: '预备铃',
    time: '07：45'
  },
  {
    name: '第一节课',
    time: '07：55－08：40'
  },
  {
    name: '第二节课',
    time: '08：45－09：30'
  },
  {
    name: '第三节课',
    time: '09：45－10：30'
  },
  {
    name: '第四节课',
    time: '10：35－11：20'
  },
  {
    name: '第五节课',
    time: '11：25－12：10'
  },
  {
    name: '中餐',
    time: '12：00－12：50'
  },
  {
    name: '预备铃',
    time: '14：30'
  },
  {
    name: '第六节课',
    time: '14：40－15：25'
  },
  {
    name: '第七节课',
    time: '15：30－16：15'
  },
  {
    name: '第八节课',
    time: '16：25－17：10'
  },
  {
    name: '第九节课',
    time: '17：20－18：05'
  },
  {
    name: '第十节课',
    time: '18：10－18：55'
  },
  {
    name: '晚餐',
    time: '18：10－19：00'
  },
  {
    name: '第十一节课',
    time: '19：30－20：15'
  },
  {
    name: '第十二节课',
    time: '20：25－21：10'
  },
  {
    name: '教学楼熄灯',
    time: '22：30'
  },
  {
    name: '宿舍楼熄灯',
    time: '23：00'
  }

];

var tgAutumn = [
  {
    name: "起床",
    time: '06:30'
  },
  {
    name: '早操',
    time: '06：40－07：00'
  },
  {
    name: '早餐',
    time: '07：00－08：00'
  },
  {
    name: '预备铃',
    time: '07：45'
  },
  {
    name: '第一节课',
    time: '07：55－08：40'
  },
  {
    name: '第二节课',
    time: '08：45－09：30'
  },
  {
    name: '第三节课',
    time: '09：45－10：30'
  },
  {
    name: '第四节课',
    time: '10：35－11：20'
  },
  {
    name: '第五节课',
    time: '11：25－12：10'
  },
  {
    name: '中餐',
    time: '12：00－12：50'
  },
  {
    name: '预备铃',
    time: '14：00'
  },
  {
    name: '第六节课',
    time: '14：10－14：55'
  },
  {
    name: '第七节课',
    time: '15：00－15：45'
  },
  {
    name: '第八节课',
    time: '15：55－16：40'
  },
  {
    name: '第九节课',
    time: '16：50－17：35'
  },
  {
    name: '第十节课',
    time: '17：40－18：25'
  },
  {
    name: '晚餐',
    time: '17：30－19：00'
  },
  {
    name: '第十一节课',
    time: '19：00－19：45'
  },
  {
    name: '第十二节课',
    time: '19：50－20：35'
  },
  {
    name: '教学楼熄灯',
    time: '22：30'
  },
  {
    name: '宿舍楼熄灯',
    time: '22：45'
  }

];
Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: ['苍梧、宋跳校区(夏季)', '苍梧、宋跳校区(秋季)', '通灌校区(夏季)', '通灌校区(秋季)'],
    index: 1,
    campus:'苍梧、宋跳校区(秋季)',
    schedules: cwAutumn


  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    if(e.detail.value == 0){
      choosedsche = cwSum;

    } else if (e.detail.value == 1){
      choosedsche = cwAutumn;

    } else if (e.detail.value == 2){
      choosedsche = tgSum;

    }else{
      choosedsche = tgAutumn;

    }

    this.setData({
      index: e.detail.value,
      campus: this.data.array[e.detail.value],
      schedules: choosedsche
    });
  }
})
