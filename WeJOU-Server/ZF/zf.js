var http = require("http");
var cheerio = require("cheerio");
var Q = require("q");
var querystring = require("querystring");
var iconv = require("iconv-lite");
const { options } = require("../app");
var ZF = {};
module.exports = ZF;

function loginStep1() {
  var defer = Q.defer();
  var url = "http://zfxk.jou.edu.cn" + "/default2.aspx";
  var req = http.get(encodeURI(url), function (req, res) {
    var html = "";
    req.on("data", function (data) {
      html += data;
    });
    req.on("end", function () {
      var $ = cheerio.load(html);
      var newUrl = $("a").attr("href");
      defer.resolve(newUrl); //获取,/(mj0nmnyyfszwtxic4aos4syt)/default2.aspx
    });
  });

  req.on("error", function (err) {
    defer.reject(err);
  });
  req.end();

  return defer.promise;
}

function loginStep2(newUrl) {
  var defer = Q.defer();
  var url = "http://zfxk.jou.edu.cn" + newUrl;
  var req = http.get(encodeURI(url), function (req, res) {
    var html = "";
    req.on("data", function (data) {
      html += data;
    });
    req.on("end", function () {
      // console.log("登录界面:"+ html);
      //获取值
      var $ = cheerio.load(html);
      var stateValue = $('#form1 > input[type="hidden"]').val();
      var obj = new Object();
      var randomUrl = newUrl.replace("/default2.aspx", ""); //获取token
      // console.log("randomUrl:"+randomUrl);
      obj.token = randomUrl;
      obj.stateValue = stateValue;
      defer.resolve(obj);
    });
  });

  req.on("error", function (err) {
    defer.reject(err);
  });
  req.end();

  return defer.promise;
}

function zfLogin(reqData) {
  var defer = Q.defer();
  var postData = querystring.stringify({
    __VIEWSTATE: reqData.stateValue,
    txtUserName: reqData.txtUserName,
    //Textbox1: "",
    TextBox2: reqData.password,
    txtSecretCode: reqData.verifyCode,
    RadioButtonList1: "%D1%A7%C9%FA",
    Button1: "",
    lbLanguage: "",
    hidPdrs: "",
    hidsc: "",
  });
  var options = {
    hostname: "zfxk.jou.edu.cn",
    port: 80,
    path: reqData.randomUrl + "/default2.aspx",
    method: "POST",
    headers: {
      //'Content-Type':'application/x-www-form-urlencoded',
      "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
      "Content-Length": Buffer.byteLength(postData),
    },
  };

  console.log("path:" + options.path);
  var req = http.request(options, function (res) {
    // console.log('Status:', res.statusCode);
    // console.log('headers:', JSON.stringify(res.headers));
    res.setEncoding("utf8");
    var html = "";
    res.on("data", function (data) {
      html += data;
    });
    res.on("end", function () {
      //console.log("返回结果:" + html);
      var obj = {};
      if (res.statusCode == 302) {
        var $ = cheerio.load(html);
        var movedToUrl = $("h2 a").attr("href");
        // console.log("movedToUrl:"+movedToUrl);
        obj.status = "success";
        obj.movedToUrl = movedToUrl;
        defer.resolve(obj);
      } else if (res.statusCode == 404) {
        obj.status = "fail";
        obj.reason = "验证码或密码不正确";
        // console.log("reason:"+ obj.reason);
        defer.resolve(obj);
      } else {
        obj.status = "maintain";
        defer.resolve(obj);
      }
    });
  });
  req.on("error", function (err) {
    console.error(err);
    defer.reject(err);
  });
  req.write(postData);
  req.end();

  return defer.promise;
}

function goToMain(mainUrl) {
  var defer = Q.defer();
  var url = "http://zfxk.jou.edu.cn" + mainUrl;
  console.log("mainUrl:" + url);
  var req = http.get(encodeURI(url), function (req, res) {
    // var html = '';
    var arrBuf = [];
    var bufLength = 0;
    req.on("data", function (data) {
      // html += data;
      arrBuf.push(data);
      bufLength += data.length;
    });
    req.on("end", function () {
      // console.log("登录界面:"+ html);
      var datas = [];
      //获取值
      var chunkAll = Buffer.concat(arrBuf, bufLength);
      var html = iconv.decode(chunkAll, "gb2312"); // 汉字不乱码
      var $ = cheerio.load(html);
      var items = $("#headDiv > ul > li:nth-child(5) > ul").find("a"); //x信息查询下的4个主要功能
      //循环items
      items.each(function (index, elem) {
        var item = {};
        item.name = $(this).text();
        item.url = $(this).attr("href");
        //console.log("menu:" + JSON.stringify(item));
        datas.push(item);
      });

      var result = {};
      result.randomUrl = mainUrl.split("/")[1];
      result.datas = datas;
      result.url = url;
      console.log(result);
      defer.resolve(result);
    });
  });

  req.on("error", function (err) {
    defer.reject(err);
  });
  req.end();

  return defer.promise;
}

function getXSCJState(result) {
  var defer = Q.defer();
  var randomUrl = result.randomUrl;
  var url = result.datas[1].url;
  var totalUrl = result.url;
  // var contentUrl = "/" + randomUrl + "/" + 'xscj_gc.aspx?xh=2016124174&xm=%D3%DA%C2%BD%C2%BD&gnmkdm=N121605';
  var contentUrl = "/" + randomUrl + "/" + url;
  //console.log("contentUrl:"+contentUrl);
  //console.log("totalUrl:"+totalUrl);
  var options = {
    hostname: "zfxk.jou.edu.cn",
    port: 80,
    path: encodeURI(contentUrl),
    method: "GET",
    headers: {
      //'Content-Type':'application/x-www-form-urlencoded',
      Referer: encodeURI(totalUrl),
      Host: "zfxk.jou.edu.cn",
    },
  };
  var req = http.get(options, function (req, res) {
    // var html = '';
    var arrBuf = [];
    var bufLength = 0;
    req.on("data", function (data) {
      // html += data;
      arrBuf.push(data);
      bufLength += data.length;
    });
    req.on("end", function () {
      // console.log("登录界面:"+ html);
      var datas = [];
      //获取值
      var chunkAll = Buffer.concat(arrBuf, bufLength);
      var html = iconv.decode(chunkAll, "gb2312"); // 汉字不乱码
      var $ = cheerio.load(html);
      // console.log("getXSCJState:"+html);
      // var stateValue = $('#form1 > input[type="hidden"]').val();
      var xscjState = $('#Form1 > input[type="hidden"]').val();
      result.xscjState = xscjState;
      //console.log(result);
      defer.resolve(result);
    });
  });

  req.on("error", function (err) {
    defer.reject(err);
  });
  req.end();

  return defer.promise;
}

function getScores(params) {
  var randomUrl = params.randomUrl;
  var mainUrl = params.url;
  var url = params.datas[1].url;
  var xscjState = params.xscjState;

  var contentUrl = "/" + randomUrl + "/" + url;

  console.log("contentUrl2:" + contentUrl);
  console.log("mainUrl2:" + mainUrl);
  var defer = Q.defer();
  var postData = querystring.stringify({
    __VIEWSTATE: xscjState,
    ddlXN: "",
    ddlXQ: "",
    Button2: "%D4%DA%D0%A3%D1%A7%CF%B0%B3%C9%BC%A8%B2%E9%D1%AF", //%D4%DA%D0%A3%D1%A7%CF%B0%B3%C9%BC%A8%B2%E9%D1%AF
  });
  // var contentUrl = "/" + randomUrl + "/" + 'xscj_gc.aspx?xh=2016124174&xm=%D3%DA%C2%BD%C2%BD&gnmkdm=N121605';
  var options = {
    hostname: "zfxk.jou.edu.cn",
    port: 80,
    path: encodeURI(contentUrl),
    method: "POST",
    headers: {
      //'Content-Type':'application/x-www-form-urlencoded',
      "User-Agent":
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36",
      "Content-Type": "application/x-www-form-urlencoded",
      "Content-Length": Buffer.byteLength(postData),
      Referer: encodeURI(mainUrl),
    },
  };

  // console.log("path:"+options.path);
  var req = http.request(options, function (res) {
    // console.log('Status:', res.statusCode);
    // console.log('headers:', JSON.stringify(res.headers));
    // var html = '';
    var arrBuf = [];
    var bufLength = 0;
    res.on("data", function (data) {
      // html += data;
      arrBuf.push(data);
      bufLength += data.length;
    });
    res.on("end", function () {
      var chunkAll = Buffer.concat(arrBuf, bufLength);
      var html = iconv.decode(chunkAll, "gb2312"); // 汉字不乱码
      //console.log("返回结果:" + html);
      var catag = [];
      var scores = [];
      var $ = cheerio.load(html);
      var items = $("#Datagrid1 > tbody").children("tr");
      items.each(function (index, element) {
        if (index == 0) {
          $(this)
            .children("td")
            .each(function (i, e) {
              catag.push($(this).text());
            });
        } else {
          var score = [];
          $(this)
            .children("td")
            .each(function (i, e) {
              score.push($(this).text());
            });
          scores.push(score);
        }
      });

      // var name = $('#Datagrid1 > tbody > tr:nth-child(2) > td:nth-child(4)').text();
      var scoreinfo = {};
      scoreinfo.status = "success";
      scoreinfo.catag = catag;
      scoreinfo.scores = scores.reverse();
      params.scoreinfo = scoreinfo;
      //console.log("params" + JSON.stringify(params));
      defer.resolve(params);
    });
  });
  req.on("error", function (err) {
    console.error(err);
    defer.reject(err);
  });
  req.write(postData);
  req.end();

  return defer.promise;
}

function gettable(params) {
  var randomUrl = params.randomUrl;
  var mainUrl = params.url;
  var url = params.datas[0].url;

  var contentUrl = "/" + randomUrl + "/" + url;

  var defer = Q.defer();

  var options = {
    hostname: "zfxk.jou.edu.cn",
    port: 80,
    path: encodeURI(contentUrl),
    method: "GET",
    headers: {
      //'Content-Type':'application/x-www-form-urlencoded',
      "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
      Referer: mainUrl,
    },
  };

  var req = http.get(options, function (res) {
    var arrBuf = [];
    var bufLength = 0;
    res.on("data", function (data) {
      // html += data;
      arrBuf.push(data);
      bufLength += data.length;
    });
    res.on("end", function () {
      var chunkAll = Buffer.concat(arrBuf, bufLength);
      var html = iconv.decode(chunkAll, "gb2312"); // 汉字不乱码
      var $ = cheerio.load(html);
      var aa = [];
      var bb = [];
      var rows = $("#Table1 tbody").children("tr").length;
      for (var i = 0; i < rows; i++) {
        //console.log($("#Table1 tbody tr").eq(i).children("td").length);
        for (
          var j = 0;
          j < $("#Table1 tbody tr").eq(i).children("td").length;
          ++j
        ) {
          var JsonData = new Object();
          JsonData.content = $("#Table1 tbody tr")
            .eq(i)
            .children("td")
            .eq(j)
            .html()
            .trim();
          if (
            "&nbsp;" != JsonData.content &&
            JsonData.content.search("<br>") != -1
          ) {
            //console.log(JsonData.content);
            aa.push(JsonData);
          }
        }
      }
      //console.log(aa);

      for (var i = 0; i < aa.length; ++i) {
        bb.push(
          aa[i].content.split("<br>").filter(function (s) {
            return s && s.trim();
          })
        );
      }
      //var obj = JSON.stringify(aa);
      //console.log(bb);

      var weektonum = { 一: 1, 二: 2, 三: 3, 四: 4, 五: 5, 六: 1, 七: 7 };

      var last = [];

      for (var i = 0; i < bb.length; i++) {
        for (var j = 0; j < bb[i].length; j += 4) {
          var lesson = new Object();
          lesson.kcmc = bb[i][j];
          lesson.teacher = bb[i][j + 2];
          lesson.classroom = bb[i][j + 3];
          lesson.xqj = weektonum[bb[i][j + 1][1]];
          lesson.skjc = parseInt(bb[i][j + 1][3]);
          lesson.skcd = 1 + parseInt(bb[i][j + 1][5]) - lesson.skjc;
          if (isNaN(lesson.skcd)) lesson.skcd = 1;

          var start = bb[i][j + 1].indexOf("{");
          var tempweek = bb[i][j + 1].substr(start);
          //console.log(tempweek);//{第8-12周|双周}
          var reg = /\d+/g;
          var week = tempweek.match(reg); //8,12
          //console.log(week);
          var time = [];
          var min = parseInt(week[0]);
          var max = parseInt(week[1]);
          //console.log("min:" + min + " max:" + max);
          //console.log(tempweek);
          if (tempweek.search("双") != -1) {
            do {
              time.push(min);
              min += 2;
            } while (min <= max);
          } else {
            for (var k = min; k <= max; k++) {
              time.push(k);
            }
          }
          //console.log(time);
          lesson.time = time;
          last.push(lesson);
        }
      }

      //var obj = JSON.stringify(last);
      params.tableinfo = last;
      console.log(params);
      defer.resolve(params);
    });
  });
  req.on("error", function (err) {
    console.error(err);
    defer.reject(err);
  });
  //req.write(postData);
  req.end();

  return defer.promise;
}

//获取viewstate和token，传给前端
ZF.getVIEW = function (response) {
  loginStep1()
    .then(function (result) {
      console.log(result + "  hhh");
      return loginStep2(result);
    })
    .then(function (result) {
      console.log(result);
      response.writeHead(200, { "Content-Type": "text/html;charset=utf-8" });
      response.end(JSON.stringify(result));
    });
};

ZF.login = function (data, response) {
  zfLogin(data)
    .then(function (result) {
      if (result.status == "success") {
        //console.log(result);
        //跳转界面，拿到5个子菜单跳转地址
        return goToMain(result.movedToUrl);
        // response.writeHead(200, { "Content-Type": "text/html;charset=utf-8" });
        // response.end(JSON.stringify(result));
      } else {
        response.writeHead(200, { "Content-Type": "text/html;charset=utf-8" });
        response.end(JSON.stringify(result));
      }
    })
    .then(function (result) {
      //console.log("result2为："+result);
      return getXSCJState(result);
    })
    .then(function (result) {
      return getScores(result);
    })
    .then(function (result) {
      return gettable(result);
    })
    .then(function (result) {
      //console.log("????????????????????????????????");
      //console.log("result:"+JSON.stringify(result));
      response.writeHead(200, { "Content-Type": "text/html;charset=utf-8" });
      response.end(JSON.stringify(result));
    });
};

// var url = "http://zfxk.jou.edu.cn/default2.aspx";
// var req = http.get(encodeURI(url), function (req, res) {
//   var html = "";
//   req.on("data", function (data) {
//     html += data;
//   });
//   req.on("end", function () {
//     // console.log("登录界面:"+ html);
//     //获取值
//     var $ = cheerio.load(html);
//     var newUrl = $("a").attr("href");
//     console.log("newUrl:" + newUrl);
//   });
// });

// req.on("error", function (err) {
//   console.log("错误是" + err.message);
// });
// req.end();
