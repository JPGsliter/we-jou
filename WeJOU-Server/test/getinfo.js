var http = require( "http" );
 
// Utility function that downloads a URL and invokes
// callback with the data.
function download(url, callback) {
   http.get(url, function (res) {
     var data = "" ;
     res.on( 'data' , function (chunk) {
       data += chunk;
     });
     res.on( "end" , function () {
       callback(data);
     });
   }).on( "error" , function () {
     callback( null );
   });
};

var url = "http://exam.jou.edu.cn/fgquery.do?status=lowquery&tsid=2020123118";

download(url, function (data) {
    if (data) {
      console.log(data);
    }
    else console.log( "error" ); 
 });

