/*最普通version，即查即用，高级搜索型
examType=%E6%9C%9F%E6%9C%AB
&academic=2020-2021-1
&startExamDate=
&endExamDate=
&startTime=
&endTime=
&instituteId=639
&arrangeInstitute=639
&courseName=
&examUnit=%E7%BD%91%E7%BB%9C181（utf8 网络181）
*/
var querystring = require("querystring");
var http = require("http");
var request = require("request");
var cheerio = require("cheerio");
var fs = require("fs");

var postData = querystring.stringify({
  examType: "期末",
  academic: "2020-2021-1",
  startExamDate: "",
  endExamDate: "",
  startTime: "",
  endTime: "",
  instituteId: 639,
  arrangeInstitute: 639,
  courseName: "",
  examUnit: "网络181",
});
var options = {
  // host: "exam.jou.edu.cn",
  // path: '/fgquery.do?status=advanceQuery',
  method: "POST",
  headers: {
    //'Content-Type':'application/x-www-form-urlencoded',
    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
    "Content-Length": Buffer.byteLength(postData),
  },
  proxy: "127.0.0.1:8888",
};

// request.post({url:'http://exam.jou.edu.cn/fgquery.do?status=advanceQuery',postData}, function(error, response, body) {
//     console.log(error,response,body);
// });

var req = http.request(
  "http://exam.jou.edu.cn/fgquery.do?status=advanceQuery",
  options,
  (res) => {
    res.setEncoding("utf8");
    var html = "";
    res.on("data", (data) => {
      html += data;
      //console.log('内容'+data);
    });
    res.on("end", function () {
      var examList = [];
      var $ = cheerio.load(html);
      var items = $(".trclick");
      console.log("length=" + items.length);
      //循环items
      items.each(function (index, elem) {
        var exam = {};
        var detailUrl = $(this)
          .attr("onclick")
          .replace("window.open('/fgquery.do?status=examdetail&examid=", "");
        var examid = detailUrl.replace("')", "");
        exam.examid = examid;
        var examItems = $(this).children("td");
        // console.log('examid:'+examid +"===="+examItems.length);
        //循环examItems
        examItems.each(function (i, e) {
          var value = $(this).text().trim();
          switch (i) {
            case 0:
              exam.num = value;
              break;
            case 1:
              exam.subject = value;
              break;
            case 2:
              exam.date = value.split(" ")[0];
              break;
            case 3:
              exam.time = value;
              break;
            case 4:
              exam.class = value;
              break;
            case 5:
              exam.address = value;
              break;
            case 6:
              exam.teacher = value;
              break;
          }
        });
        examList.push(exam);
      });
      console.log(examList);
  
    });
    res.on("error", (err) => {
      console.log("错误是" + err.message);
    });
  }
);

req.write(postData);
req.end();
