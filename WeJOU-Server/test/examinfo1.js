/*最普通version，即查即用，普通搜索型
 */

var http = require("http");
var cheerio = require("cheerio");
var fs = require("fs");
var url = "http://exam.jou.edu.cn/fgquery.do?status=lowquery&tsid=2020123118";
var info = {};

http.get(url, (req, res) => {
  var html = "";
  req.on("data", function (data) {
    html += data;
  });
  req.on("end", function () {
    var examList = [];

    var $ = cheerio.load(html);
    var items = $(
      "#content > div > table > tbody > tr:nth-child(2) > td > table > tbody"
    ).children(".trclick");
    console.log("length=" + items.length);
    //循环items
    items.each(function (index, elem) {
      var exam = {};
      var detailUrl = $(this)
        .attr("onclick")
        .replace("window.open('/fgquery.do?status=examdetail&examid=", "");
      var examid = detailUrl.replace("')", "");
      exam.examid = examid;
      var examItems = $(this).children("td");
      // console.log('examid:'+examid +"===="+examItems.length);
      //循环examItems
      examItems.each(function (i, e) {
        var value = $(this).text().trim();
        switch (i) {
          case 0:
            exam.num = value;
            break;
          case 1:
            exam.subject = value;
            break;
          case 2:
            exam.date = value.split(" ")[0];
            break;
          case 3:
            exam.time = value;
            break;
          case 4:
            exam.class = value;
            break;
          case 5:
            exam.address = value;
            break;
          case 6:
            exam.teacher = value;
            break;
        }
      });
      examList.push(exam);
    });
    console.log(examList);
    // fs.appendFile("文件.txt", info, (err) => {
    //   if (err) throw err;
    //   console.log("数据已被追加到文件");
    // });
  });
});
