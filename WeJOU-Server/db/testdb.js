var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function (err, db) {
  if (err) throw err;
  var dbo = db.db("WEJou");
  var whereStr = {"date":new Date("Thu Jun 10 2021")}; // 查询条件
  dbo
    .collection("Articles")
    .find(whereStr)
    .toArray(function (err, result) {
      if (err) throw err;
      console.log(result);
      db.close();
    });
});
