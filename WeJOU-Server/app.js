var createError = require("http-errors");
var ZF = require("./ZF/zf.js");
var DB = require("./db/db.js")
var examService = require("./server/examArrange.js")
var express = require("express");
var bodyparser = require("body-parser");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var iconv = require("iconv-lite");
var urlencodedParser = bodyparser.urlencoded({ extended: false });

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
const { Router } = require("express");

var app = express();

app.all("*", function (req, res, next) {
  //设置允许跨域的域名，*代表允许任意域名跨域
  res.header("Access-Control-Allow-Origin", "*");
  //允许的header类型
  res.header("Access-Control-Allow-Headers", "content-type");
  //跨域允许的请求方式
  res.header("Access-Control-Allow-Methods", "DELETE,PUT,POST,GET,OPTIONS");
  if (req.method.toLowerCase() == "options") res.send(200);
  //让options尝试请求快速结束
  else next();
});

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);

app.get("/getview", function (req, res) {
  ZF.getVIEW(res);
  //res.send("hhh");
});

app.get("/getexaminfo",(req,res)=>{
  var data = req.query.xh;
  examService.getexamArrange(data,res);
  console.log(data);
});

app.get("/getarticle",(req,res)=>{
  var data = req.query.date;
  DB.getarticle(data,res);
  //console.log(data);
});

app.post("/zflogin", urlencodedParser, function (req, res) {
  var data = {
    stateValue: req.body.stateValue,
    txtUserName: req.body.txtUserName,
    password: req.body.password,
    verifyCode: req.body.verifyCode,
    RadioButtonList1: "学生",
    randomUrl: req.body.randomUrl,
  };
  console.log("data请求：" + JSON.stringify(data));

  ZF.login(data, res);
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
